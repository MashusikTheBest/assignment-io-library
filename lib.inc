%define SYSCALL_EXIT 60
%define NULL_TERM 0
%define SYSCALL_READ 0
%define SYSCALL_WRITE 1
%define OUT 1
%define IN 0
%define LINE_FEED 0xA
%define LINE_SPACE 0x20
%define LINE_TAB 0x9

section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, SYSCALL_EXIT ; кладем номер системного вызова
    syscall
     
; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax ; обнуляем регистр-аккумулятор
    .count_str: 
        cmp byte[rdi+rax],NULL_TERM ; сравниваем значение  с нулем 
        je .end_count ; выходим, если равен нулю
        inc rax ; прибавляем единицу в счетчик
        jmp .count_str ; возвращаемся в начало цикла
    .end_count:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	push rdi 
    call string_length ; получаем длину строки и сохраняем ее в rax
    pop rdi
    mov rsi, rdi ; кладем ссылку на начало строки rsi
    mov rdx, rax ; кладем длину строки в rdx
    mov rax, SYSCALL_WRITE ; кладем в в rax номер системного вызова
    mov rdi, OUT ; кладем номер файлового дескриптора - stdout 
    syscall
    ret
    



; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    xor rax, rax ; обнулея регистр-аккумулятор
    mov rdi, LINE_FEED ; кладем код перевода строки в rdi 
    ; Принимает код символа и выводит его в stdout
print_char:
    push rdi ; кладем символ на вершишу стэка, чтобы получить ссылку 
    mov rsi, rsp ; кладем ссылку на вершину стэка в rsi 
    mov rdx, 1 ; кладем длину строки 
    mov rax, SYSCALL_WRITE ; кладем номер системного вызова в rax
    mov rdi, OUT ; кладем номер файлового дескриптора - stdout
    syscall 
    pop rdi ; убираем вершину стэка, возвращаем в первоначальное состояние
    ret
    

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    push r12 ; сохраняем callee-saved регистр r12
    push r13 ; сохраняем callee-saved регистр r13
    mov r13, rsp ; сохраняем значение указателя стэка
    xor rax, rax ; обнуляем регистр-аккумулятор
    mov rax, rdi ; берем входное число
    mov r12, 10 ; будем делить на 10, каждый раз получая остаток - последнюю цифру числа
    sub rsp, 1 ; выделяем байт для того, чтобы начать запись 
    mov byte[rsp], NULL_TERM ; кладем в конец строки нуль-терминатор
    .loop:
        xor rdx,rdx ; обнуляем регистр rdx
        div r12 ; делим  данное число на 10 (содержимое r12), чтобы получить остаток - последнюю цифру
        add rdx, '0' ; получаем аски-код
        sub rsp, 1 ; выделяем байт для записи цифры
        mov [rsp], dl ; берем только младшие восемь бит и кладем в стэк 
        cmp rax,0 ; если число закончилось, то естьцелая часть равна нулю, деление прекращается
        jz .end ; прекращение деления
        jmp .loop ; возвращ
    
    .end:
        mov rdi, rsp ; возврщаем указатель стэка
        call print_string ; выводим строку
        mov rsp, r13    ;fixed
        pop r13 ; возвращаем первоначальное состояние callee-saved регистров 
        pop r12 ; возвращаем первоначальное состояние callee-saved регистров
        ret


; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    xor rax, rax ; обнуляем значение регистра-аккумулятора
    mov rax, rdi ; кладем в аккумулятор входное число
    cmp rax,0  ; узнаем, положительное ли число или отрицательное
    jns print_uint ; если число положительное, то просто выводим его
    neg rax ; берем модуль отрицательного числа (инверсия + 1)
    push rdi
    mov rdi, '-' ; кладем код минуса для записи
    push rax ; сохраняем значение аккумулятора
    call print_char ; выводим минус 
    pop rax
    pop rsi ; возвращаем аккумулятор и аналогично выводим модуль числа
    mov rdi, rax    ; fixed
   	jmp print_uint ; выводим число 
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе

string_equals:
    xor rax, rax ; Обнуляем акуумулятор
    push rbx ; сохраняем значение rbx 
    .equals:
        mov bl, byte [rsi+rax]; кладем первый символ строки
        cmp byte [rdi+rax], bl ; сравниваем с первым символом другой строки
        jne .not_equals ; если не равны переходим в not_equals
        cmp byte [rdi+rax], NULL_TERM ; узнаем, не конец ли это строки
        je .end ; если конец строки, переходим на end
        inc rax ; увеличиваем счетчик (она была модной без add)
        jmp .equals ; возвращаемся в цикл
    .not_equals:
        xor rax, rax ; обнуяем аккумулятор (так как не равны)
        pop rbx ; возвращаем rbx 
        ret 
    .end:
        mov rax, 1 ; возвращаем единицу, так как строки равны
        pop rbx ; возврващем состояние rbx 
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    mov rax, SYSCALL_READ; обнуляем регистр-аккумулятор, кладем номер системного вызова 
    mov rdi, IN; номер файлового дескриптора : 0 - stdin
    sub rsp, 1; выделяем память для символа  
    mov byte [rsp], 0 ; заранее кладем нолик // fixme
    mov rsi, rsp ; кладем буфер, куда будет записан символ 
    mov rdx, 1 ; кладем размер, который мы будем читать 
    syscall
    mov rax, [rsp]; сохраняем символ или нолик, если конец потока
    inc rsp ; возвращаяем состояние стэка 
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor rax, rax
    push rbx ; сохраняем значение callee-saved регистра
    push r12 ; сохраняем значение callee-saved регистра
    mov r12, rsi ; кладем размер буфера
    xor rbx, rbx ; обнулем rbx, так как это счетчик 

.first_empty_char:
    push rdi
    push rsi
    call read_char ; читаем первый символ
    pop rsi
    pop rdi
    cmp al, LINE_SPACE  ; сравниваем с пробелом
    je .first_empty_char ; если это пробел, переходим к следующему символу
    cmp al, LINE_TAB ; сравнивам с табом 
    je .first_empty_char  ; если таб, то переходим к следующему символу
    cmp al, LINE_FEED ; сравниваем с переводом строки
    je .first_empty_char ; если перевод строки, то переходим к следующему символу
    cmp al, NULL_TERM ; сравниваем с нуль-терминатором
    je .end_of_word ; если нуль терминатор, заканчиваем считываение

.reading_word
    mov byte[rdi+rbx], al ; кладем в буфер прочитанный символ
    inc rbx ; добавляем к счетчику единицу
    push rdi ; сохраняем rdi
    push rsi 
    call read_char ; считываем символ 
    pop rsi
    pop rdi ; возвращаем rdi 
    cmp al, LINE_SPACE  ;сравниваем с пробелом
    je .end_of_word ; если пробел, то переходим на end_of_word
    cmp al, LINE_TAB ; сравниваем с табом 
    je .end_of_word ; если это таб, то переходим на end_of_word
    cmp al, LINE_FEED; сравниваем с переводом строки
    je .end_of_word ; если перевод строки, то переходим на end_of_word
    cmp al, NULL_TERM ; сравниваем с концом строки
    je .end_of_word ; есил конец строки, то на end_of_word
    cmp rbx, r12 ; сравниваем длину слова с размером буфера 
    je .end ; если одинаковые (то есть уже нет места для следующего символа и для добавления нуль-терминатора), то переходим в end
    jmp .reading_word ; продолжаем цикл 

 .end_of_word
    mov byte[rdi + rbx], NULL_TERM ; кладем нуль-терминатор
    mov rax, rdi ; кладем адрес буфера, так как считали слово
    mov rdx, rbx ; кладем длину слова 
    pop r12 ; возвращаем состояние r12
    pop rbx ; возвращем rbx
    ret

 .end
    xor rax, rax ; возвращаем ноль в аккумуляторе, так как неудача
    pop r12 ; возвращаем r12
    pop rbx ; возвращаем rbx
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax ; обнуляем аккумулятор
    xor rdx, rdx ; обнуляем rdx
    push rbx ; сохраняем значение rbx 
    mov rbx, 10; будем умножать итог на 10 каждый раз получая новую цифру 
    push r12 ;
    push r13 ;
    xor r12, r12 ; 
    xor r13, r13 ; 

.parsing: 
    mov r12b, byte[rdi+r13] ;кладем первый символ
    cmp r12b, 0x30 ; смотрим, попадает ли этот символ в промежуток с цифрами
    jb .end ;
    cmp r12b, 0x39 ;
    ja .end
    mul rbx ; умножаем итог на 10, каждый раз когда добавляем цифру, чтобы увеличить их разрядность
    sub r12b, 0x30 ; вычитаем 30, чтобы получить число, а не код
    add rax, r12 ; добавляем цифру в rax
    inc r13 ; увеличиваем счетчик 
    jmp .parsing ;
    ret

.end
    mov rdx, r13 ; кладем в длину числа 
    pop r13 ; возврващем состояние r13
    pop r12 ; возвращаем состояние r12
    pop rbx ; возвращаем состояние rbx
    ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    cmp byte[rdi],'-' ; узнаем, отрицательное это число или нет
    jne parse_uint ; парсим число, если оно положительное	
    inc rdi; перерходим к следующему символу
    call parse_uint ; парсим число
    cmp rdx, 0 ; сравниваем длину числа с нулем на случай, если числа не было
    je .not_number ; если числа не было, переходим в not_number
    neg rax ; делаем число отрицательным
    inc rdx ; кладем единичку за минус
    ret

.not_number
    xor rax, rax
    ret




; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax ; обнуляем аккумулятор
    push rdi
    push rsi 
    push rdx
    call string_length ; узнаем длину строки
    pop rdx
    pop rsi
    pop rdi
    push rbx ; сохраняем rbx 
    push r12 ; сохраняем значение callee-saved регистра r12
    mov r12, rax ; сохраняем длину строки (полученную из string_length) в регистр r12 
    xor rax, rax ; обнуляем регистр rax
    cmp rdx, r12 ; сравниваем длину строки с буфером
    ja .loop ; если строка помещается, то копируем ее в буфер
    pop r12 
    pop rbx 
    ret
    
    .loop:
    mov bl, byte [rdi+rax] ; кладем символ строки в rbx
    mov byte[rsi+rax], bl ; кладем в буфер символ строки 
    cmp byte[rdi+rax], 0 ; сравниваем, если конец строки
    je .end_copy ; если конец, выходим, возвращаем длину
    inc rax 
    jmp .loop

    .end_copy:
    mov rax, r12
    pop r12
    pop rbx
    ret
 

